import { Space, Space19, Space31, Space42, Space6, SpaceBetween50to55, SpaceMultipleOf6, Space58, Space63, SpaceGreaterThan63} from "core/domain/SpaceBoard";

test("User move space 1", () => {
  let space = new Space(1)
  const activity = space.returnActivity()

  expect(activity).toBe("Stay in space 1")
})

test("User move space 6", () => {
  let space = new Space6(6)
  const activity = space.returnActivity()

  expect(activity).toBe("The Bridge: Go to space 12")
})

test("User move space multiple of 6", () => {
  let space = new SpaceMultipleOf6(12)
  const activity = space.returnActivity()

  expect(activity).toBe("Move two spaces forward.")
})

test("User move space 19", () => {
  let space = new Space19(19)
  const activity = space.returnActivity()

  expect(activity).toBe("The Hotel: Stay for (miss) one turn.")
})

test("User move space 31", () => {
  let space = new Space31(31)
  const activity = space.returnActivity()

  expect(activity).toBe("The Well: Wait until someone comes to pull you out - they then take your place.")
})

test("User move space 42", () => {
  let space = new Space42(42)
  const activity = space.returnActivity()

  expect(activity).toBe("The Maze: Go back to space 39.")
})

test("User move space between 50 to 55", () => {
  let space = new SpaceBetween50to55(53)
  const activity = space.returnActivity()

  expect(activity).toBe("The Prison: Wait until someone comes to release you - they then take your place.")
})

test("User move space between 50 to 55", () => {
  let space = new SpaceBetween50to55(50)
  const activity = space.returnActivity()

  expect(activity).toBe("The Prison: Wait until someone comes to release you - they then take your place.")
})

test("User move space 58", () => {
  let space = new Space58(58)
  const activity = space.returnActivity()

  expect(activity).toBe("Death: Return your piece to the beginning - start the game again.")
})

test("User move space 63", () => {
  let space = new Space63(63)
  const activity = space.returnActivity()

  expect(activity).toBe("Finish: you ended the game.")
})

test("User move space greater than 63", () => {
  let space = new SpaceGreaterThan63(64)
  const activity = space.returnActivity()

  expect(activity).toBe("Move to space 53 and stay in prison for two turns.")
})