import checkSpaceBoard from "core/services/SpaceBoard"

test("User move space 1", () => {
    let result = checkSpaceBoard(1)
    expect(result).toBe("Stay in space 1")
})

test("User move space 2", () => {
    let result = checkSpaceBoard(2)
    expect(result).toBe("Stay in space 2")
})

test("User move space 6", () => {
    let result = checkSpaceBoard(6)
    expect(result).toBe("The Bridge: Go to space 12")
})

test("User move space 12", () => {
    let result = checkSpaceBoard(12)
    expect(result).toBe("Move two spaces forward.")
})

test("User move space 19", () => {
    let result = checkSpaceBoard(19)
    expect(result).toBe("The Hotel: Stay for (miss) one turn.")
})

test("User move space 42", () => {
    let result = checkSpaceBoard(42)
    expect(result).toBe("The Maze: Go back to space 39.")
})

test("User move space 53", () => {
    let result = checkSpaceBoard(53)
    expect(result).toBe("The Prison: Wait until someone comes to release you - they then take your place.")
})

test("User move space 50", () => {
    let result = checkSpaceBoard(50)
    expect(result).toBe("The Prison: Wait until someone comes to release you - they then take your place.")
})

test("User move space 55", () => {
    let result = checkSpaceBoard(55)
    expect(result).toBe("The Prison: Wait until someone comes to release you - they then take your place.")
})

test("User move space 49", () => {
    let result = checkSpaceBoard(49)
    expect(result).toBe("Stay in space 49")
})

test("User move space 58", () => {
    let result = checkSpaceBoard(58)
    expect(result).toBe("Death: Return your piece to the beginning - start the game again.")
})

test("User move space 63", () => {
    let result = checkSpaceBoard(63)
    expect(result).toBe("Finish: you ended the game.")
})

test("User move space 64", () => {
    let result = checkSpaceBoard(64)
    expect(result).toBe("Move to space 53 and stay in prison for two turns.")
})