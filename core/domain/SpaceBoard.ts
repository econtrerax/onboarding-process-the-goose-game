export abstract class SpaceBoard {
    spaceNumber: number

    constructor(spaceNumber: number){
        this.spaceNumber = spaceNumber;
    }

    abstract returnActivity(): string
}

export class Space extends SpaceBoard{
    
    returnActivity() {
        return `Stay in space ${this.spaceNumber}`; 
    }
}

export class Space6 extends SpaceBoard{
    
    returnActivity() {
        return `The Bridge: Go to space 12`; 
    }
}

export class SpaceMultipleOf6 extends SpaceBoard{
    
    returnActivity() {
        return "Move two spaces forward."
    }
}

export class Space19 extends SpaceBoard{
    
    returnActivity() {
        return "The Hotel: Stay for (miss) one turn."
    }
}

export class Space31 extends SpaceBoard{
    
    returnActivity() {
        return "The Well: Wait until someone comes to pull you out - they then take your place."
    }
}

export class Space42 extends SpaceBoard{
    
    returnActivity() {
        return "The Maze: Go back to space 39."
    }
}

export class SpaceBetween50to55 extends SpaceBoard{
    
    returnActivity() {
        return "The Prison: Wait until someone comes to release you - they then take your place."
    }
}

export class Space58 extends SpaceBoard{
    
    returnActivity() {
        return "Death: Return your piece to the beginning - start the game again."
    }
}

export class Space63 extends SpaceBoard{
    
    returnActivity() {
        return "Finish: you ended the game."
    }
}

export class SpaceGreaterThan63 extends SpaceBoard{
    
    returnActivity() {
        return "Move to space 53 and stay in prison for two turns."
    }
}