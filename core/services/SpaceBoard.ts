import { Space, SpaceMultipleOf6, Space6, Space19, Space31, Space42, SpaceBetween50to55, Space58, Space63, SpaceGreaterThan63 } from "core/domain/SpaceBoard";

export default function checkSpaceBoard(spaceNumber: number): string{
    
    if (spaceNumber === 6){
        const space = new Space6(spaceNumber);
        return space.returnActivity(); 
    }

    if (spaceNumber === 19){
        const space = new Space19(spaceNumber);
        return space.returnActivity(); 
    }

    if (spaceNumber === 31){
        const space = new Space31(spaceNumber);
        return space.returnActivity(); 
    }

    if (spaceNumber === 42){
        const space = new Space42(spaceNumber);
        return space.returnActivity(); 
    }

    if (spaceNumber >= 50 && spaceNumber <=55){
        const space = new SpaceBetween50to55(spaceNumber);
        return space.returnActivity(); 
    }

    if (spaceNumber === 58){
        const space = new Space58(spaceNumber);
        return space.returnActivity(); 
    }

    if (spaceNumber === 63){
        const space = new Space63(spaceNumber);
        return space.returnActivity(); 
    }

    if (spaceNumber > 63){
        const space = new SpaceGreaterThan63(spaceNumber);
        return space.returnActivity(); 
    }

    if (isMultipleOfSix(spaceNumber)){
        const space = new SpaceMultipleOf6(spaceNumber);
        return space.returnActivity(); 
    }

    const space = new Space(spaceNumber);
    return space.returnActivity(); 
}

function isMultipleOfSix(x: number){
    return (x % 6) === 0
} 