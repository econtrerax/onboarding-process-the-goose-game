import checkSpaceBoard from "core/services/SpaceBoard";

const MAX_SPACES_BOARD = 63
for (let space = 1; space <= MAX_SPACES_BOARD; space++) {
    console.log(`Space ${space}: ${checkSpaceBoard(space)}`);
}
